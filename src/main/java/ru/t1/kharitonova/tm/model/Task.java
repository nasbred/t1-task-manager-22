package ru.t1.kharitonova.tm.model;

import ru.t1.kharitonova.tm.api.model.IWBS;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private String projectId = null;
    private Date created = new Date();

    public Task() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
