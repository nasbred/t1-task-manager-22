package ru.t1.kharitonova.tm.service;

import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.service.IProjectService;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.DescriptionEmptyException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId,
                          final String name,
                          final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return repository.add(userId, project);
    }

    @Override
    public Project updateById(final String userId,
                              final String id,
                              final String name,
                              final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId,
                                 final Integer index,
                                 final String name,
                                 final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= repository.getSize(userId)) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
