package ru.t1.kharitonova.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException{

    public NumberIncorrectException() {
        super("Error! Number is incorrect.");
    }

    public NumberIncorrectException(String value, Throwable cause) {
        super("Error! This value is incorrect: \"" + value + "\"", cause);
    }

    public NumberIncorrectException(final String value) {
        super("Error! This value is incorrect: \"" + value + "\"");
    }

}
