package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Register user.";
    }

    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
