package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserViewProfileCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "View current user profile.";
    }

    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID : " + user.getId());
        System.out.println("LOGIN : " + user.getLogin());
        System.out.println("FIRST NAME : " + user.getFirstName());
        System.out.println("MIDDLE NAME : " + user.getMiddleName());
        System.out.println("LAST NAME : " + user.getLastName());
        System.out.println("EMAIL : " + user.getEmail());
        System.out.println("ROLE : " + user.getRole());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
