package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Update current user profile.";
    }

    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter First Name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name:");
        final String lastName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
