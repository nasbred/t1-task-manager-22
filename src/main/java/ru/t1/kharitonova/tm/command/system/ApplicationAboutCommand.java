package ru.t1.kharitonova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Kharitonova");
        System.out.println("e-mail: akharitonova@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Display developer info.";
    }

}
