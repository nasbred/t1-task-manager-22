package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Remove user.";
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
