package ru.t1.kharitonova.tm.command.task;

import ru.t1.kharitonova.tm.enumerated.TaskSort;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "Display list tasks.";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks;
        if (sort == null) tasks = getTaskService().findAll(userId);
        else tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Task task: tasks){
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + " : " + task.getStatus());
            index++;
        }
    }

}
