package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Lock user.";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
