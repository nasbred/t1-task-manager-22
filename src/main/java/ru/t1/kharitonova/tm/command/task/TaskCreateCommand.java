package ru.t1.kharitonova.tm.command.task;

import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

}
