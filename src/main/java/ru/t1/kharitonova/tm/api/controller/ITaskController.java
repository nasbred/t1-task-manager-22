package ru.t1.kharitonova.tm.api.controller;

public interface ITaskController {
    void createTask();

    void showTaskByProjectId();

    void showTasks();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();
}
