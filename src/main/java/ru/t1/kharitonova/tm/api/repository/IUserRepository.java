package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User>{

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
