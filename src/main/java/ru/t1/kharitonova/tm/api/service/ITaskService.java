package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusById(String userId, String id, Status status);

    List<Task> findAllByProjectId(String userId, String projectId);

}
