package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project>{

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusById(String userId, String id, Status status);

}
