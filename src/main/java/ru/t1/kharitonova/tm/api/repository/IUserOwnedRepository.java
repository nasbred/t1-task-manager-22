package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends IRepository<M>{

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M add(String userId, M model);

    M removeOne(String userId, M model);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    void removeAllByUserId(String userId);

}
