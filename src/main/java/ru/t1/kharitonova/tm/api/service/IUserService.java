package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;

public interface IUserService extends IUserRepository, IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
