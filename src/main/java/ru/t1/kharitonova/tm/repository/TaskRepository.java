package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return findAll(userId)
                .stream()
                .filter(task -> task.getProjectId() != null)
                .filter(task -> task.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

}
