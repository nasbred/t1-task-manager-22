package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

}
