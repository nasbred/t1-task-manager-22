package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        return records
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return records
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records
                .stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records
                .stream()
                .anyMatch(user -> login.equals(user.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return records
                .stream()
                .anyMatch(user -> email.equals(user.getEmail()));
    }

}
